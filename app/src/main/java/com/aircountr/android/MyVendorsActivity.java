package com.aircountr.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.aircountr.android.fragment.VendorsListFragment;
import com.aircountr.android.objects.CategoryListItem;
import com.splunk.mint.Mint;

import java.util.ArrayList;
import java.util.List;

import io.karim.MaterialTabs;


/**
 * Created by gaurav on 4/29/2016.
 */
public class MyVendorsActivity extends FragmentActivity implements View.OnClickListener {
    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private MaterialTabs tabs;
    private ViewPager viewPager;
    public MaterialDialog processing;
    public Typeface REGULAR, SEMIBOLD,FONTAWESOME;
    private int numberOfLoadings=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(MyVendorsActivity.this, "058c75a4");

        setContentView(R.layout.activity_my_vendors);

        REGULAR = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(getAssets(), "ProximaNova-Semibold.ttf");
        FONTAWESOME = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        tv_pageTitle.setTypeface(SEMIBOLD);
        tabs = (MaterialTabs) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setupViewPager(viewPager, ((AircountrApplication) getApplicationContext()).mCategoryListItems);

        iv_backBtn.setOnClickListener(this);
    }

    private void setupViewPager(ViewPager viewPager, ArrayList<CategoryListItem> mDataList) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), mDataList);
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private List<CategoryListItem> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager, List<CategoryListItem> mFragmentTitleList) {
            super(manager);
            this.mFragmentTitleList = mFragmentTitleList;
        }

        @Override
        public Fragment getItem(int position) {
            VendorsListFragment mVendorsListFragment = new VendorsListFragment();
            mVendorsListFragment.mVendorsList = this.mFragmentTitleList.get(position).getVendorsDataList();
            mVendorsListFragment.categoryId = this.mFragmentTitleList.get(position).getCategoryId();
            mVendorsListFragment.categoryName = this.mFragmentTitleList.get(position).getCategoryName();
            return mVendorsListFragment;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position).getCategoryName().toUpperCase();
        }
    }

    public void showDialog(String title, String msg, String btnText) {
        AlertDialog alertdialog = new AlertDialog.Builder(this).create();
        alertdialog.setCancelable(false);
        alertdialog.setTitle(title);
        alertdialog.setMessage(msg);
        alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, btnText, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertdialog.show();
    }

    public void showLoading(String message, boolean cancelable) {

        numberOfLoadings++;
        if(processing==null) {
            processing = new MaterialDialog.Builder(this)
                    //.title(R.string.progress_dialog)
                    .content(message)
                    .progress(true, 0)
                    .backgroundColorRes(R.color.white)
                    .contentColor(getResources().getColor(R.color.black))
                    .cancelable(cancelable)
                    .show();
            processing.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                }
            });
            processing.setCanceledOnTouchOutside(cancelable);
            if (!processing.isShowing()) {
                processing.show();
            }
        }
    }

    public void hideLoading() {
        if(numberOfLoadings>0)
            numberOfLoadings--;
        if (processing != null&&numberOfLoadings==0)
            processing.dismiss();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_backBtn:
                MyVendorsActivity.this.finish();
                break;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        AircountrApplication.getInstance().trackScreenView(TAG);
        numberOfLoadings=0;
        if(processing!=null)
            processing.dismiss();

    }
}