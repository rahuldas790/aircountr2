package com.aircountr.android.objects;

/**
 * Created by gaura on 5/21/2016.
 */
public class ExpenseChartDataItem {
    String name;
    String amount;
    float percentage;
    int color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public void setColor(int color){this.color=color;}

    public int getColor(){return color;}
}
