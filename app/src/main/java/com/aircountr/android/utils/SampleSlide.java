package com.aircountr.android.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aircountr.android.R;

/**
 * Created by VikramV on 7/28/2016.
 */
public class SampleSlide extends Fragment {
    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    private ImageView mImageView;

    public static SampleSlide newInstance(int layoutResId) {
        SampleSlide sampleSlide = new SampleSlide();
        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        sampleSlide.setArguments(args);

        return sampleSlide;
    }

    private int layoutResId;

    public SampleSlide() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(ARG_LAYOUT_RES_ID))
            layoutResId = getArguments().getInt(ARG_LAYOUT_RES_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(layoutResId, container, false);

        mImageView = (ImageView) root.findViewById(R.id.imgVw_tutorial);
        switch(layoutResId) {
            case 2130968645:
                mImageView.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.tutorial01, 300, 300));
                break;
            case 2130968646:
                mImageView.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.tutorial02, 300, 300));
                break;
            case 2130968647:
                mImageView.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.tutorial03, 300, 300));
                break;
            case 2130968648:
                mImageView.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.tutorial04, 300, 300));
                break;
            case 2130968649:
                mImageView.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.tutorial05, 300, 300));
                break;
            case 2130968650:
                mImageView.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.tutorial06, 300, 300));
                break;
        }

        return root;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}