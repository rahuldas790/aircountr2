package com.aircountr.android.utils;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.UUID;

/**
 * Created by VikramV on 6/26/2016.
 */
public class AircountrUtils {
    public static String getFileName(String filepath) {
        if (filepath == null)
            return null;
        final String[] filepathParts = filepath.split("/");

        return filepathParts[filepathParts.length - 1];
    }

    public static String writeToFileAndGetPath(Context context, String data) throws IOException {
        OutputStreamWriter outputStreamWriter = null;

        try {
            File outputFile = File.createTempFile(UUID.randomUUID().toString(),
                    "postBody", context.getCacheDir());
            outputStreamWriter = new OutputStreamWriter(new FileOutputStream(outputFile));
            outputStreamWriter.write(data);

            return outputFile.getAbsolutePath();

        } finally {
            if (outputStreamWriter != null) {
                try {
                    outputStreamWriter.close();
                } catch (Exception ignored) {
                }
            }
        }
    }
}