package com.aircountr.android;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.prefrences.AppPreferences;
import com.splunk.mint.Mint;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by gaura on 6/4/2016.
 */
public class WalletActivity extends BaseActivity {

    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private TextView tv_amount;
    private TextView tv_expiryDate;
    private Button bAddMoney;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(WalletActivity.this, "058c75a4");

        setContentView(R.layout.activity_wallet);

        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        tv_amount = (TextView) findViewById(R.id.tv_amount);
        tv_expiryDate = (TextView) findViewById(R.id.tv_expiryDate);
        TextView tvCurrentBal= (TextView) findViewById(R.id.tv_current_bal);
        TextView tvExpireTitle= (TextView) findViewById(R.id.tv_expire_title);
        TextView tvAddMoneyTilte= (TextView) findViewById(R.id.tv_add_CountrMoney);
        TextView tvR_599= (TextView) findViewById(R.id.tv_r_599);
        TextView tvR_999= (TextView) findViewById(R.id.tv_r_999);
        TextView tvR_1999= (TextView) findViewById(R.id.tv_r_1999);
        TextView tvCurrent_plan= (TextView) findViewById(R.id.tv_current_plan);
        TextView tvUseRecharge= (TextView) findViewById(R.id.tv_use_recharge);
        TextView tvTnc= (TextView) findViewById(R.id.tv_tnc);
        bAddMoney=(Button)findViewById(R.id.b_addMoney);

        tvCurrentBal.setTypeface(REGULAR);
        tvExpireTitle.setTypeface(REGULAR);
        tvAddMoneyTilte.setTypeface(REGULAR);
        tvR_599.setTypeface(REGULAR);
        tvR_999.setTypeface(REGULAR);
        tvR_1999.setTypeface(REGULAR);
        tvUseRecharge.setTypeface(REGULAR);
        tvCurrent_plan.setTypeface(REGULAR);
        tvTnc.setTypeface(REGULAR);
        bAddMoney.setTypeface(REGULAR);




        tv_pageTitle.setTypeface(SEMIBOLD);
        tv_amount.setTypeface(SEMIBOLD);
        tv_expiryDate.setTypeface(REGULAR);

        iv_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WalletActivity.this.finish();
            }
        });

        sendWalletRequest();

    }

    private void sendWalletRequest() {
        if (!AppPreferences.getMerchantId(WalletActivity.this).equals("") && !AppPreferences.getAccessToken(WalletActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new GetWalletAsync().execute();
            } else
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        } else {
            showDialog("Alert", "You seems to be logged out", "OK");
        }
    }

    private class GetWalletAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz Wait", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("gfkf", s);
            if (s != null) {
                try {
                    JSONObject response = new JSONObject(s);
                    boolean _success = response.getBoolean("success");
                    if (_success) {
                        JSONObject userObj = response.getJSONObject("user");
                        String _coins = userObj.getString("coins");
                        String _expiryDate = userObj.getString("expiryDate");
                        String year=_expiryDate.substring(0,4);
                        tv_amount.setText(_coins);
                        tv_expiryDate.setText(" "+ getDate(_expiryDate)+"  "+year);
                    } else {
                        String msg = response.getString("msg");
                        showDialog("Alert", msg, "OK");
                        tv_expiryDate.setText("");
                        tv_amount.setText("No Data Found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.getWalletUrl(AppPreferences.getMerchantId(WalletActivity.this));
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(WalletActivity.this));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }

        private String getDate(String time) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date postDate = null;
            try {
                postDate = sdf.parse(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String date =  DateUtils.formatDateTime(WalletActivity.this, postDate.getTime(), DateUtils.FORMAT_SHOW_DATE);

            return date;
        }

    }
}