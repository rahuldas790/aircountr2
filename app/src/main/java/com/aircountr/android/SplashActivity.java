package com.aircountr.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.aircountr.android.prefrences.AppPreferences;
import com.pushbots.push.Pushbots;
import com.splunk.mint.Mint;

public class SplashActivity extends AppCompatActivity {
    TextView loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(SplashActivity.this, "058c75a4");

        setContentView(R.layout.activity_splash);

        final Handler handler = new Handler();
        loading = (TextView) findViewById(R.id.loading);

        //Initialising notifications as soon as user sign in to homeActivity
        Pushbots.sharedInstance().init(this);
        String var = Pushbots.sharedInstance().regID();
        Log.d("token123", var);
        AppPreferences.setRegestrationToken(SplashActivity.this, var);

        Thread timer = new Thread() {
            public void run() {
                try {
                    //sleep(1000);
                    handler.post(new Runnable() {
                        public void run() {
                            loading.setText("Checking Login credentials");
                        }
                    });
                    Log.i("Rahul", "Stage 1");
                    sleep(600);
                    if (!AppPreferences.getAccessToken(SplashActivity.this).equals("")) {
                        handler.post(new Runnable() {
                            public void run() {
                                loading.setText("Login credentials found");
                            }
                        });

                        Log.i("Rahul", "Stage 2");
                        sleep(600);
                        handler.post(new Runnable() {
                            public void run() {
                                loading.setText("Scanning bills..");
                            }
                        });

                        Log.i("Rahul", "Stage 3");
                        sleep(600);
                        handler.post(new Runnable() {
                            public void run() {
                                loading.setText("Analysing business health..");
                            }
                        });
                        Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                        Log.i("Rahul", "Stage 4");
                    } else {
                        handler.post(new Runnable() {
                            public void run() {
                                loading.setText("Login credentials not found");
                            }
                        });
                        Intent i = new Intent(SplashActivity.this, SignInActivity.class);
                        startActivity(i);
                        finish();
                        Log.i("Rahul", "Stage 5");
                    }

                } catch (Exception e) {
//                    Toast.makeText(SplashActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }/* finally {
                    Intent i = new Intent(SplashActivity.this, SignInActivity.class);
                    startActivity(i);
                    finish();
                }*/
            }
        };
        timer.start();

    }
}
