package com.aircountr.android.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aircountr.android.R;
import com.aircountr.android.objects.InvoiceListItem;
import com.aircountr.android.objects.TouchImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by gaura on 6/2/2016.
 */
public class InvoiceListAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<InvoiceListItem> mDataList;
    private Typeface REGULAR, SEMIBOLD;

    public InvoiceListAdapter(Context mContext, ArrayList<InvoiceListItem> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Semibold.ttf");
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void onDataSetChanged(ArrayList<InvoiceListItem> mDataList) {
        this.mDataList = mDataList;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.row_invoice_list, null);

        final int pos = position;

        final ImageView iv_invoiceImg = (ImageView) convertView.findViewById(R.id.iv_invoiceImg);
        TextView tv_vendorName = (TextView) convertView.findViewById(R.id.tv_vendorName);
        TextView tv_categoryName = (TextView) convertView.findViewById(R.id.tv_categoryName);
        TextView tv_comment = (TextView) convertView.findViewById(R.id.tv_comment);
        TextView tv_amount = (TextView) convertView.findViewById(R.id.tv_amount);
        TextView tv_timestamp = (TextView) convertView.findViewById(R.id.tv_timeStamp);

        tv_vendorName.setTypeface(SEMIBOLD);
        tv_categoryName.setTypeface(REGULAR);
        tv_comment.setTypeface(REGULAR);
        tv_timestamp.setTypeface(REGULAR);
        tv_amount.setTypeface(SEMIBOLD);

        if (!mDataList.get(position).getVendorName().equals("")) {
            tv_vendorName.setText(mDataList.get(position).getVendorName());
            tv_amount.setText(mContext.getResources().getString(R.string.txt_rupee) + mDataList.get(position).getInvoiceAmount());
        } else {
            tv_vendorName.setText("Processing..");
        }
        tv_categoryName.setText(mDataList.get(position).getCategoryName());
        tv_comment.setText(mDataList.get(position).getComment());
        tv_timestamp.setText(mDataList.get(position).getCreatedDate().substring(0, 10));

        if (!TextUtils.isEmpty(mDataList.get(position).getImageUrl())) {
            Picasso.with(mContext).load("http://" + mDataList.get(position).getImageUrl().trim()).into(iv_invoiceImg);
        } else iv_invoiceImg.setImageResource(R.drawable.ic_logo);

        iv_invoiceImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog nagDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                nagDialog.setCancelable(false);
                nagDialog.setContentView(R.layout.pop_up_image_preview);
                Button btnClose = (Button) nagDialog.findViewById(R.id.btnIvClose);
                TouchImageView ivPreview = (TouchImageView) nagDialog.findViewById(R.id.iv_preview_image);
                if (mDataList.get(pos).getImageUrl() != null && !mDataList.get(pos).getImageUrl().equals("")) {
                    Picasso.with(mContext).load("http://" + mDataList.get(pos).getImageUrl().trim()).into(ivPreview);
                } else ivPreview.setImageResource(R.drawable.ic_logo);

                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        nagDialog.dismiss();
                    }
                });
                nagDialog.show();
            }
        });

        return convertView;
    }
}
