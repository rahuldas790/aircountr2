package com.aircountr.android.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aircountr.android.AircountrApplication;
import com.aircountr.android.EditInvoiceActivity;
import com.aircountr.android.R;
import com.aircountr.android.constants.UrlConstants;
import com.aircountr.android.objects.InvoiceListItem;
import com.aircountr.android.prefrences.AppPreferences;
import com.android.volley.toolbox.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by VOJJALA TEJA on 30-06-2016.
 */
public class MyInvoicesAdapter extends RecyclerView.Adapter<MyInvoicesAdapter.MyInvoicesViewHolder> {

    ArrayList<InvoiceListItem> mInvoiceListItems;
    Context mContext;
    ImageLoader imageLoader = AircountrApplication.getInstance().getImageLoader();
    public Typeface REGULAR,SEMIBOLD,FONTAWESOME;
    public final int EDIT_INVOICE_REQUEST=200;

    public MyInvoicesAdapter(Context mContext,ArrayList<InvoiceListItem> mInvoiceListItems){
        this.mContext = mContext;
        this.mInvoiceListItems = mInvoiceListItems;
        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Semibold.ttf");
        FONTAWESOME = Typeface.createFromAsset(mContext.getAssets(), "fontawesome-webfont.ttf");
    }

    public  class MyInvoicesViewHolder extends RecyclerView.ViewHolder{
        LinearLayout ll;
        ImageView imageView;
        TextView tv_timeStamp;
        TextView tv_amount;
        TextView tv_comment;
        TextView tv_vendorName;
        TextView tv_categoryName;
        TextView tv_icon;

        public MyInvoicesViewHolder(View itemView) {
            super(itemView);
            ll = (LinearLayout)itemView.findViewById(R.id.ll_myInv);
            imageView = (ImageView)itemView.findViewById(R.id.iv_invoiceImg);
            tv_timeStamp = (TextView)itemView.findViewById(R.id.tv_timeStamp);
            tv_amount = (TextView)itemView.findViewById(R.id.tv_amount);
            tv_comment = (TextView)itemView.findViewById(R.id.tv_comment);
            tv_vendorName = (TextView)itemView.findViewById(R.id.tv_vendorName);
            tv_categoryName = (TextView)itemView.findViewById(R.id.tv_categoryName);
            tv_icon = (TextView)itemView.findViewById(R.id.tv_icon);
            tv_timeStamp.setTypeface(REGULAR);
            tv_amount.setTypeface(REGULAR);
            tv_comment.setTypeface(REGULAR);
            tv_vendorName.setTypeface(SEMIBOLD);
            tv_categoryName.setTypeface(REGULAR);
            tv_icon.setTypeface(FONTAWESOME);
        }
        public void bindView(final int position) {
            final InvoiceListItem invoiceListItem=mInvoiceListItems.get(position);
            ll.setBackgroundColor(Color.WHITE);
            if(invoiceListItem.getImageUrl()!="") {
                StringBuilder imageUrlBuilder = new StringBuilder(invoiceListItem.getImageUrl());
                imageUrlBuilder.insert(18, UrlConstants.INVOICE_THUMBNAIL_IMAGE);
                Log.d("MyInvoicesAdapter: " , imageUrlBuilder.toString());
                //Picasso.with(mContext).load("http://" + invoiceListItem.getImageUrl()).error(R.drawable.ic_logo).fit().centerCrop().into(imageView);
                Picasso.with(mContext).load("http://" + imageUrlBuilder.toString()).error(R.drawable.ic_logo).fit().centerCrop().into(imageView);
            }
            else {

                imageView.setImageResource(R.drawable.ic_logo);
            }
            if(invoiceListItem.isProcessed()) {
                tv_icon.setText(R.string.txt_processed);
                tv_icon.setTextColor(mContext.getResources().getColor(R.color.txt_processed));
                tv_vendorName.setText(invoiceListItem.getVendorName());
                tv_comment.setText(invoiceListItem.getComment());
                tv_amount.setText(invoiceListItem.getInvoiceAmount());
            }else{
                tv_icon.setText(R.string.txt_processing);
                tv_icon.setTextColor(mContext.getResources().getColor(R.color.txt_processing));
                tv_vendorName.setText("Processing...");
                tv_comment.setText("");
                tv_amount.setText("");
            }
            tv_timeStamp.setText(invoiceListItem.getCreatedDate());
            tv_categoryName.setText(invoiceListItem.getCategoryName());
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog nagDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                    nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    nagDialog.setCancelable(false);
                    nagDialog.setContentView(R.layout.pop_up_image_preview);
                    Button btnClose = (Button) nagDialog.findViewById(R.id.btnIvClose);
                    ImageView ivPreview = (ImageView) nagDialog.findViewById(R.id.iv_preview_image);
                    if (invoiceListItem.getImageUrl() != "") {
                        Log.d("ifsection","http://" + (invoiceListItem).getImageUrl());
                        Picasso.with(mContext).load("http://" + (invoiceListItem).getImageUrl()).error(R.drawable.ic_logo).resize(1000, 1000).into(ivPreview);
                    }
                    else ivPreview.setImageResource(R.drawable.ic_logo);

                    btnClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {
                            nagDialog.dismiss();
                        }
                    });
                    nagDialog.show();
                }
            });

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, EditInvoiceActivity.class);
                    intent.putExtra("createdDate",invoiceListItem.getCreatedDate());
                    intent.putExtra("merchantId", AppPreferences.getMerchantId(mContext));
                    intent.putExtra("invoiceId",invoiceListItem.getInvoiceId());
                    intent.putExtra("invoiceamount",invoiceListItem.getInvoiceAmount().substring(1));
                    intent.putExtra("comments",invoiceListItem.getComment());
                    intent.putExtra("categoryname",invoiceListItem.getCategoryName());
                    intent.putExtra("categoryId",invoiceListItem.getCategoryId());
                    intent.putExtra("vendorId",invoiceListItem.getVendorId());
                    intent.putExtra("vendorname",invoiceListItem.getVendorName());
                    intent.putExtra("invoice",invoiceListItem.getImageUrl());
                    intent.putExtra("position",position);
                    intent.putExtra("isautomode",invoiceListItem.isAutoMode());
                    ((Activity)mContext).startActivityForResult(intent, EDIT_INVOICE_REQUEST);
                }
            });

        }
    }
    @Override
    public MyInvoicesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_invoices_cardview,parent,false);
        MyInvoicesViewHolder mvh = new MyInvoicesViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final MyInvoicesViewHolder holder, final int position) {
        holder.bindView(position);
    }

    @Override
    public int getItemCount() {
        return mInvoiceListItems.size();
    }
}
