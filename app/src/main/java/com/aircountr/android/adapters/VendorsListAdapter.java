package com.aircountr.android.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aircountr.android.R;
import com.aircountr.android.objects.CategoryListItem;

import java.util.ArrayList;

/**
 * Created by gaurav on 4/29/2016.
 */
public class VendorsListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<CategoryListItem.Vendors> mDataList;
    private LayoutInflater mLayoutInflater;
    private VendorsEventClickListener mVendorsEventClickListener;
    public Typeface REGULAR;

    public VendorsListAdapter(Context mContext, ArrayList<CategoryListItem.Vendors> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void onDataSetChanged(ArrayList<CategoryListItem.Vendors> mDataList) {
        this.mDataList = mDataList;
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.row_vendors_list, null);

            viewHolder.tv_vendorName = (TextView) convertView.findViewById(R.id.tv_vendorName);
            viewHolder.tv_vendorAddress = (TextView) convertView.findViewById(R.id.tv_vendorAddress);
            viewHolder.iv_callBtn = (ImageView) convertView.findViewById(R.id.iv_callBtn);
            viewHolder.iv_whatsAppBtn = (ImageView) convertView.findViewById(R.id.iv_whatsAppBtn);
            viewHolder.ll_vendorRow = (LinearLayout)convertView.findViewById(R.id.ll_vendorRow);

            viewHolder.tv_vendorName.setTypeface(REGULAR);
            viewHolder.tv_vendorAddress.setTypeface(REGULAR);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_vendorName.setText(mDataList.get(position).getVendorName());
        viewHolder.tv_vendorAddress.setText(mDataList.get(position).getVendorAddress());
        viewHolder.iv_callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEventClickListenerValues(1, mDataList.get(position).getVendorNumber(),position);
            }
        });
        viewHolder.iv_whatsAppBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEventClickListenerValues(2,mDataList.get(position).getVendorNumber(),position);
            }
        });
        viewHolder.ll_vendorRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEventClickListenerValues(3,mDataList.get(position).getVendorNumber(),position);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        private LinearLayout ll_vendorRow;
        private TextView tv_vendorName;
        private TextView tv_vendorAddress;
        private ImageView iv_callBtn;
        private ImageView iv_whatsAppBtn;
    }

    public interface VendorsEventClickListener {
        public void onEventClickListener(int btnType, String phoneNumber,int position);
    }

    public void setVendorClickEventListener(VendorsEventClickListener mVendorsEventClickListener) {
        this.mVendorsEventClickListener = mVendorsEventClickListener;
    }

    public void setEventClickListenerValues(int btnType, String phoneNumber,int position) {
        this.mVendorsEventClickListener.onEventClickListener(btnType, phoneNumber,position);
    }
}
