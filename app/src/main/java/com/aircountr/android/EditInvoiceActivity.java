package com.aircountr.android;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.aircountr.android.adapters.CategorySpinnerAdapter;
import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.objects.CategoryListItem;
import com.aircountr.android.prefrences.AppPreferences;
import com.splunk.mint.Mint;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class EditInvoiceActivity extends AppCompatActivity implements View.OnClickListener,DatePickerDialog.OnDateSetListener, View.OnFocusChangeListener {
    private String TAG = this.getClass().getSimpleName();
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Toolbar toolbar;
    private MaterialDialog processing;
    private Typeface FONTAWESOME,REGULAR;
    private TextView icon_cal,icon_category,icon_rupee,icon_vendor,icon_comments,tv_errVendor;
    private EditText tv_cal,tv_category,tv_rupee,tv_comments;
    private AutoCompleteTextView tv_vendor;
    private TextInputLayout til_cal,til_category,til_rupee,til_vendor,til_comments;
    private Bundle bundle;
    private int imageBtnType = 0;
    private final int BTN_CATEGORY = 1, BTN_VENDOR = 2;
    private final int CAMERA_REQUEST = 101, GALLERY_REQUEST = 100,RESULT_CROP = 400;
    private byte[] image_byte;
    private ImageView iv_imageInv;
    private ByteArrayBody bab;
    private Uri fileUri;
    private String categoryId = "",vendorId="";
    private FloatingActionButton fab;
    DatePickerDialog dpd;
    private static final String IMAGE_DIRECTORY_NAME = "Aircountr";
    private ArrayList<CategoryListItem.Vendors> vendorsDataList;
    private ArrayList<String > mvendorsDataList = new ArrayList<>();
    private TextView updateBtn;
    private boolean isautomode=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(EditInvoiceActivity.this, "058c75a4");

        setContentView(R.layout.activity_edit_invoice);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_container);
        REGULAR = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");
        FONTAWESOME = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        setSupportActionBar(toolbar);
        collapsingToolbarLayout.setTitle("Update Invoice");
        collapsingToolbarLayout.setExpandedTitleTypeface(REGULAR);
        collapsingToolbarLayout.setCollapsedTitleTypeface(REGULAR);
        bundle=getIntent().getExtras();
        init();
    }
    private void init(){
        icon_cal=(TextView)findViewById(R.id.icon_cal);
        icon_rupee=(TextView)findViewById(R.id.icon_rupee);
        icon_category=(TextView)findViewById(R.id.icon_category);
        icon_vendor=(TextView)findViewById(R.id.icon_vendor);
        icon_comments=(TextView)findViewById(R.id.icon_comments);
        iv_imageInv=(ImageView)findViewById(R.id.iv_invoiceImg);
        fab=(FloatingActionButton)findViewById(R.id.fab);
        updateBtn=(TextView)findViewById(R.id.tv_updateBtn);
        categoryId=bundle.getString("categoryId");
        vendorId=bundle.getString("vendorId");
        isautomode=bundle.getBoolean("isautomode");
        dpd=null;

        icon_cal.setTypeface(FONTAWESOME);
        icon_rupee.setTypeface(FONTAWESOME);
        icon_category.setTypeface(FONTAWESOME);
        icon_vendor.setTypeface(FONTAWESOME);
        icon_comments.setTypeface(FONTAWESOME);

        tv_cal = (EditText)findViewById(R.id.tv_cal);
        tv_rupee = (EditText)findViewById(R.id.tv_rupee);
        tv_category = (EditText)findViewById(R.id.tv_category);
        tv_vendor = (AutoCompleteTextView)findViewById(R.id.tv_vendor);
        tv_comments = (EditText)findViewById(R.id.tv_comments);
        tv_errVendor = (TextView)findViewById(R.id.tv_errVendor);
        tv_vendor.setThreshold(1);

        tv_cal.setTypeface(REGULAR);
        tv_rupee.setTypeface(REGULAR);
        tv_category.setTypeface(REGULAR);
        tv_vendor.setTypeface(REGULAR);
        tv_comments.setTypeface(REGULAR);
        updateBtn.setTypeface(REGULAR);
        tv_errVendor.setTypeface(REGULAR);

        setStyleForTextForAutoComplete(getResources().getColor(R.color.black));
        tv_vendor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    setStyleForTextForAutoComplete(getResources().getColor(R.color.blue));
                } else {
                        setStyleForTextForAutoComplete(getResources().getColor(R.color.black));
                }
            }
        });
        String date=bundle.getString("createdDate");
        tv_cal.setText(date.substring(8,10)+"/"+date.substring(5,7)+"/"+date.substring(0,4));
        tv_rupee.setText(bundle.getString("invoiceamount"));
        if(tv_rupee.getText().toString().trim().length()==0)
            tv_rupee.setText("0");
        tv_category.setText(bundle.getString("categoryname"));
        tv_vendor.setText(bundle.getString("vendorname"));
        tv_comments.setText(bundle.getString("comments"));
        if(bundle.getString("invoice")!="")
            Picasso.with(getBaseContext()).load("http://"+bundle.getString("invoice")).error(R.drawable.ic_logo).fit().centerCrop().into(iv_imageInv);

        vendorsDataList=new ArrayList<CategoryListItem.Vendors>();
        for(int i=0;i<((AircountrApplication) getApplicationContext()).mCategoryListItems.size();i++)
        {
            if(((AircountrApplication) getApplicationContext()).mCategoryListItems.get(i).getCategoryName().equals(tv_category.getText().toString().trim())){
                vendorsDataList =(((AircountrApplication) getApplicationContext()).mCategoryListItems.get(i).getVendorsDataList());
                mvendorsDataList.clear();
                mvendorsDataList = (((AircountrApplication) getApplicationContext()).mCategoryListItems.get(i).getAllVendorsNames());
                break;
            }
        }
        ArrayAdapter<String> vendorTextAdapter = new ArrayAdapter<String>(EditInvoiceActivity.this, R.layout.row_gps_address_list, mvendorsDataList);
        tv_vendor.setAdapter(vendorTextAdapter);
        til_cal = (TextInputLayout)findViewById(R.id.til_cal);
        til_rupee = (TextInputLayout)findViewById(R.id.til_rupee);
        til_category = (TextInputLayout)findViewById(R.id.til_category);
        til_vendor = (TextInputLayout)findViewById(R.id.til_vendor);
        til_comments = (TextInputLayout)findViewById(R.id.til_comments);

        tv_cal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    datePickerDialog();
                }
                return false;
            }
        });
        tv_category.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    selectOptionPopUp(BTN_CATEGORY);
                }
                return false;
            }
        });
        fab.setOnClickListener(this);
        updateBtn.setOnClickListener(this);
        tv_vendor.setOnClickListener(this);
        fab.setBackgroundTintList(getResources().getColorStateList(R.color.white));

        tv_rupee.setOnFocusChangeListener(this);
        tv_vendor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tv_vendor.getText().toString().trim().length()==0)
                {
                    tv_errVendor.setVisibility(View.VISIBLE);
                    tv_errVendor.setText("Plz enter vendor name");
                }else{
                    tv_errVendor.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditInvoiceActivity.this.finish();
            }
        });
    }

    private void setStyleForTextForAutoComplete(int color) {
        Drawable wrappedDrawable = DrawableCompat.wrap(tv_vendor.getBackground());
        DrawableCompat.setTint(wrappedDrawable, color);
        tv_vendor.setBackgroundDrawable(wrappedDrawable);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v.getId()==R.id.tv_rupee){
            if((!hasFocus)&&tv_rupee.getText().toString().trim().length()==0){
                tv_rupee.setText("0");
            }
            else if(hasFocus){
                tv_rupee.selectAll();
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(tv_cal.hasFocus()){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(tv_cal.getWindowToken(), 0);
        }
        else if(tv_category.hasFocus()){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(tv_category.getWindowToken(), 0);
        }else if(til_category.hasFocus())
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(til_category.getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id) {
            case R.id.fab:
                selectImgOptionPopUp();
                break;
            case R.id.tv_updateBtn:
                if (tv_vendor.getText().toString().trim().length() == 0)
                {
                    tv_errVendor.setVisibility(View.VISIBLE);
                    tv_errVendor.setText("Plz enter vendor name");
                }
                else {
                    vendorId="";
                    String tv_vendorText=tv_vendor.getText().toString().trim();
                    if(vendorsDataList!=null&&vendorsDataList.size()>0)
                    {
                        for(int i=0;i<vendorsDataList.size();i++)
                        {
                            if(vendorsDataList.get(i).getVendorName().equals(tv_vendorText))
                            {
                                vendorId=vendorsDataList.get(i).getVendorId();
                            }
                        }
                    }
                    sendEditInvoiceRequest();
                }
                break;
            case R.id.tv_vendor:
                selectOptionPopUp(BTN_VENDOR);
        }
    }

    private void selectOptionPopUp(final int btnType) {
        final Dialog dialog = new Dialog(EditInvoiceActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_category_dropdown);
        ListView lv_category = (ListView) dialog.findViewById(R.id.lv_category);
        if (btnType == BTN_CATEGORY) {
            CategorySpinnerAdapter mCategorySpinnerAdapter = new CategorySpinnerAdapter(EditInvoiceActivity.this, ((AircountrApplication) getApplicationContext()).mCategoryListItems);
            lv_category.setAdapter(mCategorySpinnerAdapter);

            lv_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tv_category.setText(((AircountrApplication) getApplicationContext()).mCategoryListItems.get(position).getCategoryName());
                    categoryId = ((AircountrApplication) getApplicationContext()).mCategoryListItems.get(position).getCategoryId();
                    vendorsDataList.clear();
                    vendorsDataList =(((AircountrApplication) getApplicationContext()).mCategoryListItems.get(position).getVendorsDataList());
                    mvendorsDataList.clear();
                    mvendorsDataList = (((AircountrApplication) getApplicationContext()).mCategoryListItems.get(position).getAllVendorsNames());
                    ArrayAdapter<String> vendorTextAdapter = new ArrayAdapter<String>(EditInvoiceActivity.this, R.layout.row_gps_address_list, mvendorsDataList);
                    tv_vendor.setAdapter(vendorTextAdapter);
                    tv_vendor.setThreshold(1);
                    dialog.dismiss();
                }
            });
            if(((AircountrApplication) getApplicationContext()).mCategoryListItems!=null&&((AircountrApplication) getApplicationContext()).mCategoryListItems.size()>0)
                dialog.show();
        }
    }


    public void datePickerDialog() {
        Calendar now = Calendar.getInstance();
        if(dpd!=null)dpd=null;
        dpd = DatePickerDialog.newInstance(
                EditInvoiceActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.vibrate(false);
        dpd.dismissOnPause(true);
        dpd.showYearPickerFirst(false);
        dpd.setAccentColor(getResources().getColor(R.color.theme_color_blue));
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        tv_cal.setText(date);
        if(dpd!=null){
            dpd.dismiss();
        }
    }
    private void selectImgOptionPopUp() {
        final Dialog dialog = new Dialog(EditInvoiceActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_select_img_layout);
        TextView tv_cameraBtn = (TextView) dialog.findViewById(R.id.tv_cameraBtn);
        TextView tv_galleryBtn = (TextView) dialog.findViewById(R.id.tv_galleryBtn);
        TextView tv_cancelBtn = (TextView) dialog.findViewById(R.id.tv_cancelBtn);

        tv_cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQUEST);
                dialog.dismiss();
            }
        });

        tv_galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
                dialog.dismiss();
            }
        });

        tv_cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GALLERY_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        Uri selectedImage = data.getData();
                        Log.d("Rahul", "I: "+data.toString());
//                        performCrop(data.getStringExtra("picturePath"));
                        iv_imageInv.setImageURI(selectedImage);
                        try {
                            FileInputStream imageStream = (FileInputStream) getContentResolver().openInputStream(selectedImage);
                            Bitmap selectedImg = BitmapFactory.decodeStream(imageStream);
                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                            selectedImg.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
                            image_byte = outputStream.toByteArray();
                            bab = new ByteArrayBody(image_byte, System.currentTimeMillis() + ".jpg");
                        } catch (FileNotFoundException e) {
                            AircountrApplication.getInstance().trackException(e);
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getBaseContext(),"Selected image not found",Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
//                    performCrop(data.getStringExtra("picturePath"));
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    iv_imageInv.setImageBitmap(photo);
                    Uri tempUri = getImageUri(getApplicationContext(), photo);
                    File finalFile = new File(getRealPathFromURI(tempUri));
                    Bitmap selectedImg = BitmapFactory.decodeFile(finalFile.getAbsolutePath());
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    selectedImg.compress(Bitmap.CompressFormat.PNG, 70, outputStream);
                    image_byte = outputStream.toByteArray();
                    bab = new ByteArrayBody(image_byte, System.currentTimeMillis() + ".png");
                }
                break;

            case RESULT_CROP:
                Log.d("Rahul", resultCode+"");
                if(resultCode == Activity.RESULT_OK){
                    Bundle extras = data.getExtras();
                    Bitmap selectedBitmap = extras.getParcelable("data");
                    // Set The Bitmap Data To ImageView
                    iv_imageInv.setImageBitmap(selectedBitmap);
                    iv_imageInv.setScaleType(ImageView.ScaleType.FIT_XY);
                }

        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }

        return mediaFile;
    }

    public boolean isNetworkAvailable(){
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    private void sendEditInvoiceRequest() {
        if (!AppPreferences.getMerchantId(EditInvoiceActivity.this).equals("") && !AppPreferences.getAccessToken(EditInvoiceActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                updateBtn.setClickable(false);
                new EditInvoiceAsync().execute(tv_category.getText().toString(),tv_rupee.getText().toString(),tv_vendor.getText().toString(),tv_comments.getText().toString(),tv_cal.getText().toString());
            } else {
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
            }
        } else {
            Intent intent = new Intent(EditInvoiceActivity.this, SignInActivity.class);
            startActivity(intent);
        }
    }


    private class EditInvoiceAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz wait...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    String msg = response.getString("msg");
                    if (success) {
                        Intent intent=new Intent();
                        JSONObject jsonObject = response.getJSONObject("user");
                        intent.putExtra("position",bundle.getInt("position"));
                        intent.putExtra("invoiceId",jsonObject.optString("_id"));
                        intent.putExtra("vendorId",jsonObject.optString("vendorId"));
                        intent.putExtra("isautomode",jsonObject.optString("isautomode"));
                        intent.putExtra("categoryId",jsonObject.optString("categoryId"));
                        intent.putExtra("vendorname",jsonObject.optString("vendorname"));
                        intent.putExtra("categoryname",jsonObject.optString("categoryname"));
                        intent.putExtra("createdDate",jsonObject.optString("createdDate").substring(0,10));
                        intent.putExtra("invoiceamount",jsonObject.optString("invoiceamount"));
                        intent.putExtra("comments",jsonObject.optString("comments"));
                        intent.putExtra("isprocessed",jsonObject.optString("isprocessed"));
                        intent.putExtra("imageurl",jsonObject.optString("imageurl"));
                        displayToast(msg);
                        setResult(Activity.RESULT_OK,intent);
                        EditInvoiceActivity.this.finish();
                    } else {
                        updateBtn.setClickable(true);
                        showDialog("Aler", msg, "OK");
                    }

                } catch (JSONException e) {
                    AircountrApplication.getInstance().trackException(e);
                    e.printStackTrace();
                }
            }
            updateBtn.setClickable(true);
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.getEditInvoiceUrl();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Authorization", AppPreferences.getAccessToken(EditInvoiceActivity.this));
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            try {
                if(!vendorId.equals(""))
                    entity.addPart("vendorId", new StringBody(vendorId));
                entity.addPart("merchantId", new StringBody(AppPreferences.getMerchantId(EditInvoiceActivity.this)));
                entity.addPart("categoryId", new StringBody(categoryId));
                entity.addPart("invoiceamount", new StringBody(params[1]));
                entity.addPart("isautomode", new StringBody("false"));
                entity.addPart("comments", new StringBody(params[3]));
                entity.addPart("createdDate", new StringBody(params[4]));
                entity.addPart("vendorname", new StringBody(params[2]));
                entity.addPart("categoryname", new StringBody(params[0]));
                entity.addPart("isautomode",new StringBody(String.valueOf(isautomode)));
                entity.addPart("invoiceId", new StringBody(bundle.getString("invoiceId")));
                entity.addPart("isprocessed", new StringBody("true"));
                if (bab != null)
                    entity.addPart("invoice", bab);
                else
                    entity.addPart("invoice",new StringBody(bundle.getString("invoice")));

                httpPost.setEntity(entity);
                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                AircountrApplication.getInstance().trackException(e);
                e.printStackTrace();
            }
            Log.d(TAG, "Response : " + _response);
            return _response;
        }
    }
    public void displayToast(int id) {
        Toast.makeText(getApplicationContext(), getResources().getString(id),
                Toast.LENGTH_SHORT).show();
    }

    public void displayToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public void showDialog(String title, String msg, String btnText) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(title)
                .content(msg)
                .backgroundColorRes(R.color.white)
                .titleColorRes(R.color.black)
                .contentColor(Color.BLACK)
                .positiveColorRes(R.color.theme_color_blue)
                .buttonRippleColorRes(R.color.transparent_theme_color)
                .positiveText(btnText)
                .cancelable(false)
                .show();
    }

    public void showDialog(String title, String msg, String posBtnText, String negBtnText) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(title)
                .content(msg)
                .backgroundColorRes(R.color.white)
                .titleColorRes(R.color.black)
                .contentColor(Color.BLACK)
                .positiveColorRes(R.color.theme_color_blue)
                .buttonRippleColorRes(R.color.transparent_theme_color)
                .positiveText(posBtnText)
                .positiveColorRes(R.color.theme_color_blue)
                .buttonRippleColorRes(R.color.transparent_theme_color)
                .negativeText(negBtnText)
                .cancelable(true)
                .show();
    }

    public void showLoading(String message, boolean cancelable) {
        processing = new MaterialDialog.Builder(this)
                //.title(R.string.progress_dialog)
                .content(message)
                .progress(true, 0)
                .backgroundColorRes(R.color.white)
                .contentColor(getResources().getColor(R.color.black))
                .cancelable(cancelable)
                .show();
    }

    public void hideLoading() {
        if (processing != null)
            processing.dismiss();
    }

    private void performCrop(String picUri) {
        try {
            //Start Crop Activity
            Log.d("Rahul", "All good here");
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP);
            Log.d("Rahul", "All good here2");
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}